const express = require('express')
const db =require('../db')
const utils=require('../utils')

const router = express.Router();

router.get('/',(request,response)=>{
    const query=`select * from student`;
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    }) 
})

module.exports=router;