const express=require("express")
const cors=require("cors")

const app=express();
const routeStudent= require('./routes/student')

app.use(cors('*'))
app.use(express.json())
app.use('/student',routeStudent)



app.listen(4000,'0.0.0.0',()=>{
    console.log('server started on port no 4000')
})
